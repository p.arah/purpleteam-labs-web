+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Why Top Teams Choose PurpleTeam<br><br>"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++



<div class="row">
  <div class="col-md-12 col-lg-4">
    <div class="card" style="min-height: 10rem">
      <div class="card-body">
        <i class="fas fa-hourglass-half fa-3x" aria-hidden="true"></i>
        <p class="card-text" style="padding-left: 0;">No time wasted writing tests.</p>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-4">
    <div class="card" style="min-height: 10rem">
      <div class="card-body">
        <i class="fas fa-desktop fa-3x" aria-hidden="true"></i>
        <p class="card-text" style="padding-left: 0;">Shows your tests running in real-time.</p>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-4">
    <div class="card" style="min-height: 10rem">
      <div class="card-body">
        <i class="fas fa-directions fa-3x" aria-hidden="true"></i>
        <p class="card-text" style="padding-left: 0;">Provides test reproduction directions.</p>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-4">
    <div class="card" style="min-height: 10rem">
      <div class="card-body">
        <i class="fas fa-user-secret fa-3x" aria-hidden="true"></i>
        <p class="card-text" style="padding-left: 0;">Provides the security experts, so you don't need to be an expert.</p>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-4">
    <div class="card" style="min-height: 10rem">
      <div class="card-body">
        <i class="fas fa-tools fa-3x" aria-hidden="true"></i>
        <p class="card-text" style="padding-left: 0;">Test Outcomes include easy to follow remediation steps.</p>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-4">
    <div class="card" style="min-height: 10rem">
      <div class="card-body">
        <i class="far fa-folder-open fa-3x" aria-hidden="true"></i>
        <p class="card-text" style="padding-left: 0;">Ships your test outcomes directly to a location of your choosing.</p>
      </div>
    </div>
  </div>
</div>
