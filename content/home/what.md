+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "Don't let Security Block Your Development Teams"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-home-what"
+++

<br>
<div class="section-heading">
  <p class="text-center section-heading"><a href="https://github.com/purpleteam-labs/purpleteam-build-test-cli" target="_blank" class="lead">Automatable</a> Security Regression Testing for your Applications and APIs</p>
</div>

<!-- Similar layout to https://www.cypress.io/features/ -->

<div class="row">
  <div class="col-md-12 col-lg-5">
    <h2 class="pt-heading">Embedded Red Teamer</h2>
    <p>Works alongside Development Teams, finding security defects as they're introduced. No more waiting for the dreaded Red Team to come and test your project weeks or months after functionality and security defects have been baked in.</p>
  </div>
  <div class="col-md-12 col-lg-7 my-auto">
    {{< figure src="home/PT_home_embeddedRT.svg" theme="light" alt="embedded red teamer" class="pt-align-right" >}}
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-5 order-lg-last">
    <h2 class="pt-heading">Your Security Coach</h2>
    <p>Coaches Developers how to spot common defect patterns as they code. PurpleTeam provides details of detected security issues in real-time as well as providing a full test <a href="/doc/definitions/" target="_blank">Outcome</a> archive with test results and reports in multiple formats after every <a href="/doc/definitions/" target="_blank">Test Run</a>. Outcomes include:</p>
    <ul><li>How to find and fix the defects</li><li>How to reproduce the failing tests so you can verify the fix manually if you choose to do so</li><li>Guidance around the particular defects</li></ul>
  </div>
  <div class="col-md-12 col-lg-7 my-auto">
    {{< figure src="home/PT_home_securitycoach.svg" theme="light" alt="security coach" >}}
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-5">
    <h2 class="pt-heading">Checks TLS/SSL Configurations</h2>
    <p>Works alongside Operations Teams, finding security defects in TLS/SSL configurations. PurpleTeam tests for a large array of common security faults with TLS/SSL configurations, providing peace of mind that your TLS/SSL deployments are going to prove a challenge for even the most advanced attackers.</p>
  </div>
  <div class="col-md-12 col-lg-7 my-auto">
    {{< figure src="home/PT_home_TTLSSLconfig.svg" theme="light" alt="checks tls/ssl configurations" class="pt-align-right" >}}
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-5 order-lg-last">
    <h2 class="pt-heading">Checks Server Configurations</h2>
    <p>Works alongside both Development and Operations Teams, finding server misconfigurations. Misconfigured servers can provide fruitful reconnaissance information for attackers, as well as plentiful opportunities to exploit the servers running your application/API code due to known vulnerabilities, thus providing a foothold for attackers. With PurpleTeam watching your servers, you will be ahead of your attackers and be able to remediate known server defects before they are exploited by your adversaries.</p>
  </div>
  <div class="col-md-12 col-lg-7 my-auto">
    {{< figure src="home/PT_home_serverconfig.svg" theme="light" alt="checks server configurations" >}}
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-5">
    <h2 class="pt-heading">In Your Build Pipelines</h2>
    <p>Perfect fit for your CI/CD build pipelines We have directions on how to include PurpleTeam into your build pipelines, along with <a href="https://github.com/purpleteam-labs/purpleteam-build-test-cli" target="_blank" >example projects</a> to get you up and running fast.</p>
  </div>
  <div class="col-md-12 col-lg-7 my-auto">
    {{< figure src="home/PT_home_pipeline.svg" theme="light" alt="build (ci/cd) pipelines" class="pt-align-right" >}}
  </div>
</div>
