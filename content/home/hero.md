+++
# Hero widget.
widget = "hero"
headless = true  # This file represents a page section.
active = true
date = 2021-05-23

title = "Modern Development Teams move fast - Choose a Security Solution that can keep up"

# Order that this section will appear in.
weight = 10

# Hero image (optional). Enter filename of an image in the page folder.
#hero_media = 'logo.png'

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  #color = "#000"
  
  # Background gradient.
  # gradient_start = "#4bb4e3"
  # gradient_end = "#2b94c3"
  
  # Background image.
  # Todo: Use animated image
  image = "home/speed-skate.jpg"  # Name of image in `static/media/`.
  #image_darken = 0.7  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["30rem", "0px", "40rem", "0"]

# Call to action button (optional).
#   Activate the button by specifying a URL and button label below.
#   Deactivate by commenting out parameters, prefixing lines with `#`.
[cta]
  url = '/product/'
  label = "Get Started"
  icon_pack = "fas"
  icon = "running"
[cta_alt]
  url = '/doc/'
  label = "View Documentation"
# Note. An optional note to show underneath the links.
[cta_note]
  label = '<br><br>Purpleteam Your Applications and APIs with PurpleTeam<br><br>'
+++

<br><br>

