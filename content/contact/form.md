+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["5rem", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

<!-- For css use: https://getbootstrap.com/docs/3.4/css/ -->
<!-- We're using bootstrap v4.6.0 -->

<!-- In order to debug custom scripts, you need to put them in the /static/js/ directory and reference them as per the below line -->
<!-- <script language="javascript" type="text/javascript"  src="/js/pt-custom.js"></script> -->
<!-- Also mentioned in assets/js/READ.txt -->

<!-- Other email form options are: https://getsimpleform.com/ https://formspark.io/ (which supports redirect and requires one off payment, although has no email domain spoofing protection) -->

<section id="contact" class="home-section wg-contact  ">
  <div class="home-section-bg "></div>
    <div class="container">
      <div class="row  ">
        <div class="section-heading col-12 col-lg-4 mb-3 mb-lg-0 d-flex flex-column align-items-center align-items-lg-start">
          <h1 class="mb-0">Contact Us</h1>
        </div>
        <div class="col-12 col-lg-8">
          <div class="mb-3">
            <form name="contact" class="pt-contact" method="POST" action="">
              <input type="hidden" name="_redirect" value="#contact-submitted"><!-- This would work with formspark, but not with formspree free -->
              <div class="form-action-value" hidden>https://formspree.io/f/mjvjyywa</div>
              <div class="form-group form-inline">
                <label class="sr-only" for="inputName">Full Name</label>
                <input type="text" name="name" class="form-control w-100" id="inputName" placeholder="Full Name" required/>
              </div>
              <div class="form-group form-inline">
                <label class="sr-only" for="inputEmail">Email</label>
                <input type="email" name="email" class="form-control w-100" id="inputEmail" placeholder="Email" required/>
              </div>
              <div class="form-group form-inline">
                <label class="sr-only" for="inputMessage">Message</label>
                <textarea name="message" class="form-control w-100" id="inputMessage" rows="5" placeholder="Message" required></textarea>
              </div>
              <label class="pt-post-contact-field label">Help fight spam. Enter the correct answer in the next field *</label>
              <label class="pt-post-contact-field js-note label">JavaScript must be enabled to see question.</label>
              <div class="form-group">
                <input type="text" name="validateRealContactUser" class="pt-post-contact-field form-control" required/>
              </div>
              <div class="d-none">
                <label>Do not fill this field unless you are a bot: <input name="welcome-bot"></label>
              </div>
              <button type="submit" class="btn btn-outline-primary px-3 py-2">Send</button>
            </form>
          </div>
          <ul class="fa-ul">
            <li>
              <i class="fa-li fas fa-envelope fa-2x" aria-hidden="true"></i>
              <span id="person-email"><a href="mailto:info@purpleteam-labs.com">info@purpleteam-labs.com</a></span>
            </li>
            <li>
              <i class="fa-li fas fa-phone fa-2x" aria-hidden="true"></i>
              <span id="person-telephone"><a href="tel:+64%20274%20622%20607">(+64) 274 622 607</a></span>
            </li>
            <!--<li>
              <i class="fa-li fas fa-map-marker fa-2x" aria-hidden="true"></i>
              <span id="person-address">450 Serra Mall, Stanford, CA, 94305</span>
            </li>
            <li>
              <i class="fa-li fas fa-compass fa-2x" aria-hidden="true"></i>
              <span>Enter Building 1 and take the stairs to Office 200 on Floor 2</span>
            </li>-->
            <li>
              <i class="fa-li fas fa-clock fa-2x" aria-hidden="true"></i>
              <span>Monday - Friday 10:00 to 18:00 NZT</span>
            </li>
            <!--<li>
              <i class="fa-li fas fa-calendar-check fa-2x" aria-hidden="true"></i>
              <a href="https://calendly.com" target="_blank" rel="noopener">Book an appointment</a>
            </li>-->
            <li>
              <i class="fa-li fab fa-twitter fa-2x" aria-hidden="true"></i>
              <a href="https://twitter.com/purpleteamlabs" target="_blank" rel="noopener">DM Us</a>
            </li>
          </ul>
          <div id="contact-submitted" class="dialog"><!-- Not used with Formspree free -->
            <h3>Thank you</h3>
            <p>Your message has been submitted.</p>
            <p>We will contact you as soon as possible.</p>
            <p><a href="#" class="btn btn-primary comment-buttons ok">OK</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


