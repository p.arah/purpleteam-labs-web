+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "PurpleTeam..."
subtitle = "<br>"
draft = false

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-why-purpleteam-fix"
+++

<!-- 3. Fix -->

<div class="row">  
  <div class="col-sm-12 col-md-7 align-self-top">
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-2">
          {{< figure src="why-purpleteam/PT_why_watch_105.svg" theme="light" alt="Watches over your teams security" >}}
        </div>
        <div class="col-10">
          <div class="card-body">
            <p class="card-text" style="padding: 0;">Watches over your Teams security as they code, alerting on security defects as they're introduced.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-0 col-md-5 align-self-top">
  </div>
</div>
<div class="row">
  <div class="col-sm-0 col-md-1 align-self-top">
  </div>
  <div class="col-sm-12 col-md-7 align-self-top">
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-2">
          {{< figure src="why-purpleteam/PT_why_coach_105.svg" theme="light" alt="Coaches your team" >}}
        </div>
        <div class="col-10">
          <div class="card-body">
            <p class="card-text" style="padding: 0;">Coaches your Team by gently showing them what security defects look like.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-0 col-md-4 align-self-top">
  </div>
</div>
<div class="row">
  <div class="col-sm-0 col-md-2 align-self-top">
  </div>
  <div class="col-sm-12 col-md-7 align-self-top">
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-2">
          {{< figure src="why-purpleteam/PT_why_recognise_105.svg" theme="light" alt="Explains how to fix" >}}
        </div>
        <div class="col-10">
          <div class="card-body">
            <p class="card-text" style="padding: 0;">Explains how to fix these security defects and recognise them for the future.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-0 col-md-3 align-self-top">
  </div>
</div>
<div class="row">
  <div class="col-sm-0 col-md-3 align-self-top">
  </div>
  <div class="col-sm-12 col-md-7 align-self-top">
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-2">
          {{< figure src="why-purpleteam/PT_why_smart_105.svg" theme="light" alt="No more writing tests" >}}
        </div>
        <div class="col-10">
          <div class="card-body">
            <p class="card-text" style="padding: 0;">No more writing security tests. PurpleTeam is smart enough to know how to test.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-0 col-md-2 align-self-top">
  </div>
</div>
<div class="row">
  <div class="col-sm-0 col-md-4 align-self-top">
  </div>
  <div class="col-sm-12 col-md-7 align-self-top">
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-2">
          {{< figure src="why-purpleteam/PT_why_retest_105.svg" theme="light" alt="Retests" >}}
        </div>
        <div class="col-10">
          <div class="card-body">
            <p class="card-text" style="padding: 0;">Retests once you have been guided through applying the fixes.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-0 col-md-1 align-self-top">
  </div>
</div>
<div class="row">
  <div class="col-sm-0 col-md-5 align-self-top">
  </div>
  <div class="col-sm-12 col-md-7 align-self-top">
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-2">
          {{< figure src="why-purpleteam/PT_why_manual_105.svg" theme="light" alt="Shows how to reproduce tests" >}}
        </div>
        <div class="col-10">
          <div class="card-body">
            <p class="card-text" style="padding: 0;">Shows how to reproduce the tests manually.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-0 col-md-0 align-self-top">
  </div>
</div>
<br>
<br>
<p class="text-center">You no longer have to <span class="lead">kick the security can</span> down the Product Backlog.<br>PurpleTeam makes understanding and fixing your security defects so easy.<br>You can fix your defects as they're introduced, in the current Sprint, today.</p>
