+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "Built by Security Engineers"
subtitle = "<br>All with backgrounds as Software Engineers."
draft = false

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  image = "home/PT_header_light02.png"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["30rem", "0", "40rem", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-why-purpleteam-why-pt"
+++

<div class="row">
  <div class="col-8 align-self-top">
    <p>PurpleTeam-Labs have industry experience working in both Blue Teams and Red Teams. This gives us a unique (both sides of the fence) perspective on how your attackers think, along with the challenges you as a defender face daily. PurpleTeam is a combination of both Red and Blue teams working together distilled into a product that understands the security challenges you face daily.</p>
  </div>
  <div class="col-4 align-self-top" style="padding-right: 0;">
    <ul class="list-group list-group-flush">
      <li class="list-group-item text-left pt-what-you-get pt-line-item" style="background: transparent; border-width: 0;">
        <span><i class="fa fa-check" aria-hidden="true"></i></span><div style="color: #fff">Built by Blue Teamers</div>
      </li>
      <li class="list-group-item text-left pt-what-you-get pt-line-item" style="background: transparent; border-width: 0;">
        <span><i class="fa fa-check" aria-hidden="true"></i></span><div style="color: #fff">Built by Red Teamers</div>
      </li>
      <li class="list-group-item text-left pt-what-you-get pt-line-item" style="background: transparent; border-width: 0;">
        <span><i class="fa fa-check" aria-hidden="true"></i></span><div style="color: #fff">Understand Your Security Challenges</div>
      </li>
    </ul>
  </div>
</div>
