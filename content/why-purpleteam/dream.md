+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "PurpleTeam strikes at the very heart of this problem"
subtitle = "<br>"
draft = false

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-why-purpleteam-dream"
+++

<!-- 2. Dream -->

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        &nbsp;
        <div class="row">
          <div class="col-8 my-auto">
            <p>Imagine if you could have the Red Team sitting with your Development and Operations Teams watching as they code, discovering security issues as they're created, at the cheapest place to find and fix. Studies have shown that this cost reduction is a factor of <a href="https://f0.holisticinfosecforwebdevelopers.com/chap06.html#leanpub-auto-cheapest-place-to-deal-with-defects" target="_blank">10-25 times</a> the cost of finding and fixing defects with traditional Red Teaming.</p>
          </div>
          <div class="col-4 my-auto">
            <div class="card">
              {{< figure src="why-purpleteam/PT_whySquare_25x.svg" theme="light" alt="10-25 times the cost" class="card-img" >}}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-4 my-auto">
            <div class="card">
              {{< figure src="why-purpleteam/PT_whySquare_100.svg" theme="light" alt="100% of security defects fixed" class="card-img" >}}
            </div>
          </div>
          <div class="col-8 my-auto">
            <p>Experience shows that only <a href="https://speakerdeck.com/binarymist/security-regression-testing-on-owasp-zap-node-api?slide=58" target="_blank">14%</a> of security defects found in a traditional Red Team exercise are actually fixed due to the remediation cost/effort. What if <a href="https://speakerdeck.com/binarymist/security-regression-testing-on-owasp-zap-node-api?slide=58" target="_blank">100%</a> of those security defects could be fixed?</p>
          </div>
        </div>
        <div class="row">
          <div class="col-8 my-auto">
            <p>Now imagine if you could automate the above process. Thus saving the cost of the actual Red Team as well.</p>
          </div>
          <div class="col-4 my-auto">
            <div class="card">
              {{< figure src="why-purpleteam/PT_whySquare_auto.svg" theme="light" alt="saving the cost of the red team" class="card-img" >}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>






