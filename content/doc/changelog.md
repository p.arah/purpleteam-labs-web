---
title: Changelog
linktitle: Changelog
summary: Changelogs for the purpleteam component projects.
toc: true
type: book
date: "2021-05-12T00:00:00+01:00"
draft: false

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 150
---

<br>The following are references to the changelogs for the core PurpleTeam component projects.

* [CLI](https://github.com/purpleteam-labs/purpleteam/releases "purpleteam CLI release notes")
* [Orchestrator](https://github.com/purpleteam-labs/purpleteam-orchestrator/releases "purpleteam orchestrator release notes")
* [Application Tester](https://github.com/purpleteam-labs/purpleteam-app-scanner/releases "purpleteam application scanner release notes")
* [TLS Tester](https://github.com/purpleteam-labs/purpleteam-tls-scanner/releases "purpleteam tls scanner release notes")
* [Server Tester](https://github.com/purpleteam-labs/purpleteam-server-scanner/releases "purpleteam server scanner release notes")
* [Lambda Functions](https://github.com/purpleteam-labs/purpleteam-lambda/releases "purpleteam lambda release notes")
* [Stage Two Containers](https://github.com/purpleteam-labs/purpleteam-s2-containers/releases "purpleteam stage two containers release notes")
* [IAC SUT](https://github.com/purpleteam-labs/purpleteam-iac-sut/releases "purpleteam Infrastructure as Code - System Under Test")
* [build test CLI](https://github.com/purpleteam-labs/purpleteam-build-test-cli/releases "purpleteam build test CLI")
* [logger](https://github.com/purpleteam-labs/purpleteam-logger/releases "purpleteam logger")
* [website](https://gitlab.com/purpleteam-labs/purpleteam-labs-web/-/blob/main/CHANGELOG.md)

The rest of the PurpleTeam changelogs can be found at the [OWASP project](https://owasp.org/www-project-purpleteam/).

