---
title: API Job File Structure
linktitle: └─ API
summary: Targeting Web APIs.
toc: true
type: book
date: "2021-11-27T00:00:00+01:00"
draft: false

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 110
---

The following explains the _Job_ file structure targeting web APIs.

<br>

{{< toc hide_on="xl" >}}

---

## `data`

A single [resource object] containing the following:

### `type`

The type defines the resource and which schema that will be used. The only valid value for this schema is `Api`.

### `attributes`

An [attributes object] containing the following:

#### `version`

The schema version that the _Job_ file must conform to.

#### `sutAuthentication`

An object containing the following properties:

{{< highlight js >}}
"emissaryAuthenticationStrategy": String,
"route": String
{{< /highlight >}}

* `emissaryAuthenticationStrategy` - (Optional, Default: `MaintainJwt`) One of [ `MaintainJwt` ]. The specific strategy used defines how the _Emissary_ should be authenticated to the _SUT_. The default strategy for the `Api` schema adds the base URL (`sutIp`) to the _Emissary_'s Context with an appended `.*` so that everything under the base URL (`sutIp`) of the API definition you provide will be within the _Emissary_ scan Context. If you want less scanned you will need to break the API definition up into smaller definitions per _Test Session_. If you need a [different behaviour](../#strategy-properties) discuss with us what you need
* `route` - A `String` specifying the route to authenticate to the _SUT_, conforming to the relevant regular expression in the _Job_ schema

#### `sutIp`

A valid IP or hostname (`String`) for the _SUT_ you are targeting.

#### `sutPort`

A valid port (`Number`) for the _SUT_ you are targeting.

#### `sutProtocol`

(Optional, Default: `https`) One of [ `http` | `https` ]. The only valid value for the TLS Tester is `https`.

#### `loggedInIndicator`

(Optional) Sub-string to expect in a future response indicating that the _Emissary_ was successfully authenticated.
See the [Zaproxy documentation](https://www.zaproxy.org/docs/desktop/start/features/authentication/) for additional details.
Conflicts with `loggedOutIndicator`

#### `loggedOutIndicator`

(Optional) Sub-string to expect as part of a future response indicating that the _Emissary_ is not currently authenticated.
See the [Zaproxy documentation](https://www.zaproxy.org/docs/desktop/start/features/authentication/) for additional details.
Conflicts with `loggedInIndicator`

### `relationships`

A [relationships object] containing a `data` ([resource linkage]) array of [resource identifier object]s. In the case of _PurpleTeam_, these refer to one of [ `tlsScanner` | `appScanner` | `serverScanner` ({{< hl >}}coming soon{{< /hl >}})] [resource object]s.

#### `data`

An array of [resource identifier object]s containing the following properties that identify the _Test Session_ [resource object]s to be included in the _Test Run_:

{{< highlight js >}}
{
  "type": ["tlsScanner"|"appScanner"],
  "id": String
}
{{< /highlight >}}

* `type` - One of [ `tlsScanner` | `appScanner` | `serverScanner` ({{< hl >}}coming soon{{< /hl >}})]
* `id` - The id of the `type` _Test Session_ [resource object] being included

</br>

---

## `included`

An array of [resource object]s.

Such as _Test Sessions_ (`tlsScanner`, `appScanner`, `serverScanner` ({{< hl >}}coming soon{{< /hl >}})) and `route`. 

### `tlsScanner`

The only valid number of `tlsScanner` [resource object]s is one.

This is handled in the _orchestrator's_ _Tester_ models. A meaningful error will be generated and returned to the CLI if this number is not correct. Part of the _orchestrator's_ initialisation phase of each _Tester_ is to validate that the number of their _Test Session_ [resource object]s fits within the allowed range. If they don't then none of the _Testers_ will be started.

#### `type`

The only valid value is `tlsScanner` which defines the [resource object].

#### `id`

The only valid value is `NA`.

#### `attributes`

An [attributes object] containing the following:

`tlsScannerSeverity`

(Optional) One of [ `LOW` | `MEDIUM` | `HIGH` | `CRITICAL` ].

If the TLS _Tester_ finds a severity equal to or higher than the `tlsScannerSeverity` value, then it will be logged to reports: CSV and JSON only, as well as to the Graphical CLI bug counter. `WARN` is another level which translates to a client-side scanning error or problem. `WARN` and `DEBUG` records will always be seen in all reports if they occur.

`INFO` and `OK` records are only logged to CSV and JSON reports if the `tlsScannerSeverity` property doesn't exist. They are however logged to LOG and HTML reports regardless.

`OK`, `INFO`, `WARN` and `DEBUG` records are not counted as defects.

All TLS _Tester_ actions get logged to LOG and HTML reports, as well as to the CLI output. The CLI log for the TLS _Tester_ is the same as the TLS _Tester_ report but with some extra details about whether or not the _Test Run_ was a pass or a fail.
If it was a fail the CLI log for the TLS _Tester_ will provide details around how many vulnerabilities exceeded the _Build User_ defined alert threshold.

`alertThreshold`

(Optional) If present it's value must be between 0 and 9999. This property is useful for expressing the number of defects you expect to be found, any defects over and above this number will fail the _Test Run_.
If the `alertThreshold` exists in the _Job_ file, it allows the _Build User_ to ignore alerts for the sake of a _Test Run_ passing.

### `appScanner`

The only valid number of `appScanner` [resource object]s is from 1 to 12 inclusive.

This is handled in the _orchestrator's_ _Tester_ models. A meaningful error will be generated and returned to the CLI if this number is not correct. Part of the _orchestrator's_ initialisation phase of each _Tester_ is to validate that the number of their _Test Session_ [resource object]s fits within the allowed range. If they don't then none of the _Testers_ will be started.

#### `type`

The only valid value is `appScanner` which defines the [resource object].

#### `id`

A `String` that conforms to the relevant regular expression in the _Job_ schema.

#### `attributes`

An [attributes object] containing the following:

`sitesTreePopulationStrategy`

(Optional, Default: `ImportUrls`) One of [ `ImportUrls` | `OpenApi` | `Soap` | `GraphQl` ]. The specific strategy used defines how the _Emissary_ sites tree is populated

`spiderStrategy`

(Optional, Default: `Standard`) One of [ `Standard` ]. The specific strategy used defines how the _Emissary_ spider scans the _SUT_

`scannersStrategy`

(Optional, Default: `ApiStandard`) One of [ `ApiStandard` ]. The specific strategy used defines how the _Emissary_ scanners are configured. The default strategy for the `Api` schema loads only a sub-set of the scanners used for web applications as there is no DOM. It also adds a couple of scripts.

`scanningStrategy`

(Optional, Default: `ApiStandard`) One of [ `ApiStandard` ]. The specific strategy used defines how the _Emissary_ carries out it's scanning. Using the default strategy for the `Api` schema both the _Emissary_'s spider and active scanner will scan the base URL (`sutIp`) recursively. If you want less scanned, you will need to break the API definition up into smaller definitions per _Test Session_. If you need a [different behaviour](../#strategy-properties) discuss with us what you need

`postScanningStrategy`

(Optional, Default: `ApiStandard`) One of [ `ApiStandard` ]. The specific strategy used defines how the _Emissary_ carries out it's immediate post scanning activities

`reportingStrategy`

(Optional, Default: `Standard`) One of [ `Standard` ]. The specific strategy used here defines how the _Emissary_ carries out it's post scanning reporting activities

`reports`

(Optional) object containing the single property `templateThemes` array. If not present all report types will be generated. See [templateThemes](#templatethemes) below for more details.

{{% callout note %}}
Just be aware that the AWS API Gateway only allows responses of 10MB maximum, if the outcomes archive is larger than that, then you will receive a generic error: `Error occurred while downloading the outcomes file, error was: purpleteam Cloud API responded with "orchestrator is down"` due to a `500` error from API Gateway. If this happens, reduce the set of reports you specify.
{{% /callout %}}

`username`

A `String` used to authenticate to your _SUT_ and/or for various other calls to the _Emissary_ API. Must conform to the relevant regular expression in the _Job_ schema.

{{% callout note %}}
Either `openApi` or `soap` or `graphQl` or `importUrls` must be specified.
{{% /callout %}}

`openApi`

(Optional) Object containing one of [ `importFileContentBase64` | `importUrl` ]. See [openApi](#openapi) below for more details. Conflicts with `soap`, `graphQl` and `importUrls`.

`soap`

(Optional) Object containing one of [ `importFileContentBase64` | `importUrl` ]. See [soap](#soap) below for more details. Conflicts with `openApi`, `graphQl` and `importUrls`.

`graphQl`

(Optional) Object containing one of [ `importFileContentBase64` | `importUrl` ], as well as options to override the default behaviour of the _Emissary_. See [graphQl](#graphql) below for more details. Conflicts with `openApi`, `soap` and `importUrls`.

`importUrls`

(Optional) Object containing one of [ `importFileContentBase64` ]. See [importUrls](#importurls) below for more details. Conflicts with `openApi`, `soap` and `graphQl`.

`aScannerAttackStrength`

(Optional, Default: `HIGH`) The default is specified in the App _Tester_ config file. One of [ `LOW` | `MEDIUM` | `HIGH` | `INSANE` ]. For APIs we usually recommend setting this to `MEDIUM`.

`aScannerAlertThreshold`

(Optional, Default: `LOW`) The default is specified in the App _Tester_ config file. One of [ `LOW` | `MEDIUM` | `HIGH` ]

`alertThreshold`

(Optional, Default: `0`) If present it's value must be between 0 and 9999. This property is useful for expressing the number of defects you expect to be found, any defects over and above this number will fail the _Test Run_.
If the `alertThreshold` exists in the _Job_ file, it allows the _Build User_ to ignore alerts for the sake of a _Test Run_ passing.

`excludedRoutes`

(Optional) Array of regular expression strings containing route sub-strings to be added to the _Emissary_'s "Exclude from Context" list.
These can be quite useful if you have decided to create an `appScanner` _Test Session_ with no associated `route` [resource object]s which means the App _Emissary_ will be scanning from `sutIp` recursively including all resources under it.

`updateAlertsConfidence` {{< hl >}}[coming soon](https://github.com/purpleteam-labs/purpleteam/issues/100){{< /hl >}}

(Optional) Array containing objects of [ `confidenceId` & `filter` ] properties. See [updateAlertsConfidence](#updatealertsconfidence) below for more details.

### `serverScanner`

{{< hl >}}coming soon{{< /hl >}}

</br>

---

## Sub Resources

The following resources are referenced from the above _Job_ schema.

### `templateThemes`

An array containing one or more of the following report meta data objects you expect to be generated by the _Emissary_.
This allows the _Build User_ to specify which report types they desire:

{{< highlight js >}}
{
  "name": String
}
{{< /highlight >}}

* `name` - One of [ `traditionalHtml` | `traditionalHtmlPlusLight` | `traditionalHtmlPlusDark` | `traditionalJson` | `traditionalMd` | `traditionalXml` | `riskConfidenceHtmlDark` | `modernMarketing` | `highLevelReport` ]

{{< spoiler text="Base64 encoding API definition file contents." >}}
On a Linux system you can encode a text file by following these directions:

1. `base64 --wrap=0 [plain-text-file].json > [encoded-file]`
2. If you would like to check the results:  
  `base64 --decode [encoded-file] > [decoded-plain-text-file]`
3. Diff your original `[plain-text-file].json` with the `[decoded-plain-text-file]`
4. Add the contents of the `[encoded-file]` to the `importFileContentBase64` value of your _Job_ file:  
   1. `cat [encoded-file]`
   2. Copy the printed text but not the `%` at the end
   3. Using VSCode (or your preferred text editor), paste the copied text into the property value of the _Job_ file
   4. Check the _Job_ file with VSCode or Vim


{{< /spoiler >}}

### `openApi`

An object containing the following properties:

{{< highlight js >}}
"importFileContentBase64": String,
"importUrl": String
{{< /highlight >}}

* `importFileContentBase64` - (Optional) The base64 encoded file contents of your API definition. Conflicts with `importUrl`
* `importUrl` - (Optional) The URL to your API definition. Conflicts with `importFileContentBase64`

### `soap`

An object containing the following properties:

{{< highlight js >}}
"importFileContentBase64": String,
"importUrl": String
{{< /highlight >}}

* `importFileContentBase64` - (Optional) The base64 encoded file contents of your API definition. Conflicts with `importUrl`
* `importUrl` - (Optional) The URL to your API definition. Conflicts with `importFileContentBase64`

### `graphQl`

An object containing the following properties:

{{< highlight js >}}
"importFileContentBase64": String,
"importUrl": String
{{< /highlight >}}

* `importFileContentBase64` - (Optional) The base64 encoded file contents of your API definition. Conflicts with `importUrl`
* `importUrl` - (Optional) The URL to your API definition. Conflicts with `importFileContentBase64`
* `maxQueryDepth` - (Optional, Default: `5`) Specifies the maximum query generation depth
* `maxArgsDepth` - (Optional, Default: `5`) Specifies the maximum arguments generation depth
* `optionalArgsEnabled` - (Optional, Default: `true`) Boolean that specifies whether or not Optional Arguments should be specified
* `argsType` - (Optional, Default: `BOTH`) One of [ `INLINE` | `VARIABLES` | `BOTH` ]. Specifies how arguments are specified
* `querySplitType` - (Optional, Default: `LEAF`) One of [ `LEAF` | `ROOT_FIELD` | `OPERATION` ]. Specifies the level for which a single query is generated
* `requestMethod` - (Optional, Default: `POST_JSON`) One of [ `POST_JSON` | `POST_GRAPHQL` \ `GET` ]. Specifies the request method

### `importUrls`

An object containing the following properties:

{{< highlight js >}}
"importFileContentBase64": String
{{< /highlight >}}

* `importFileContentBase64` - The base64 encoded file contents of the URLs. The file contents before encoding must be plain text with one URL per line

### `updateAlertsConfidence`

{{< hl >}}[coming soon](https://github.com/purpleteam-labs/purpleteam/issues/100){{< /hl >}}

An array containing none to many objects with the following properties. This allows the _Build User_ to define none to many sets of a filter to apply a confidence level to. A given alert raised by the _Emissary_ must have all of the properties matching a given filter in order for the specified confidence to be applied:

{{< highlight js >}}
"confidenceId": integer,
"filter": {
  "name": String,
  "cweid": String, 
  "wascid": String,
  "confidence": String,
  "risk": String,
  "description": String,
  "url": String
}
{{< /highlight >}}

* `confidenceId` - One of [ `0` | `1` | `2` | `3` | `4` ]. `0`: False Positive, `1`: Low, `2`: Medium, `3`: High, `4`: _Build User_ Confirmed
* `filter` - An object containing none to all of the following properties. None means there is no filter so act on all alerts:
  * `name` - (Optional) The name of the alert to filter on
  * `cweid` - (Optional) The cweid of the alert to filter on
  * `wascid` - (Optional) The wascid of the alert to filter on
  * `confidence` - (Optional) The existing confidence of the alert to filter on
  * `risk` - (Optional) The current risk of the alert to filter on
  * `description` - (Optional) The description of the alert to filter on
  * `url` - (Optional) The url of the alert to filter on






[resource object]: https://jsonapi.org/format/#document-resource-objects
[attributes object]: https://jsonapi.org/format/#document-resource-object-attributes
[relationships object]: https://jsonapi.org/format/#document-resource-object-relationships
[resource linkage]: https://jsonapi.org/format/#document-resource-object-linkage
[resource identifier object]: https://jsonapi.org/format/#document-resource-identifier-objects

