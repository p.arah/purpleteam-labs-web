---
title: Browser App Job File Structure
linktitle: └─ Browser App
summary: Targeting Web applications that run in a browser.
toc: true
type: book
date: "2021-11-27T00:00:00+01:00"
draft: false

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 100
---

The following explains the _Job_ file structure targeting browser based web applications.

<br>

{{< toc hide_on="xl" >}}

---

## `data`

A single [resource object] containing the following:

### `type`

The type defines the resource and which schema that will be used. The only valid value for this schema is `BrowserApp`.

### `attributes`

An [attributes object] containing the following:

#### `version`

The schema version that the _Job_ file must conform to.

#### `sutAuthentication`

An object containing the following properties:

{{< highlight js >}}
"sitesTreeSutAuthenticationPopulationStrategy": String,
"emissaryAuthenticationStrategy": String,
"route": String,
"usernameFieldLocater": String,
"passwordFieldLocater": String,
"submit": String,
"expectedPageSourceSuccess": String
{{< /highlight >}}

* `sitesTreeSutAuthenticationPopulationStrategy` - (Optional) One of [ `FormStandard` | `Link` ]. If not present the default will be `FormStandard`. The specific strategy used here defines how the _Emissary_ sites tree is populated for the _SUT_ authentication URL. Only used for the `BrowserApp` _Job_ schema
* `emissaryAuthenticationStrategy` - (Optional, Default: `FormStandard`) One of [ `FormStandard` | `ScriptLink` ]. The specific strategy used defines how the _Emissary_ should be authenticated to the _SUT_. The default strategy for the `BrowserApp` schema checks to see if there are routes for a given _Test Session_, if there are they will each be added to the _Emissary_'s scan Context. If there are no routes for a given _Test Session_ the base URL (`sutIp`) will be added to the _Emissary_'s scan Context with a `.*` following it so that everything under the base URL (`sutIp`) will be within the _Emissary_'s scan Context. This allows the _Build User_ to get either broad scans or narrow focussed scans. If you need a [different behaviour](../#strategy-properties) discuss with us what you need.
* `route` - A `String` specifying the route to authenticate to the _SUT_, conforming to the relevant regular expression in the _Job_ schema
* `usernameFieldLocater` - (Optional) A `String` that is used to find the element on the page to enter the `username` value in order to authenticate to the _SUT_. The `String` is used to search for the first occurrence of a DOM `id`, class name, or `name` that matches the given `String`. Must conform to the relevant regular expression in the _Job_ schema
* `passwordFieldLocater` - (Optional) Similar to `usernameFieldLocater` but for the `password`
* `submit` - (Optional) Similar to `usernameFieldLocater` but to find the page element to trigger submission
* <div id="expectedPageSourceSuccess"></div> <code>expectedPageSourceSuccess</code> - A sub-string to expect as part of the response page to indicate that authentication has been successful. This is used by Selenium

#### `sutIp`

A valid IP or hostname (`String`) for the _SUT_ you are targeting.

#### `sutPort`

A valid port (`Number`) for the _SUT_ you are targeting.

#### `sutProtocol`

(Optional, Default: `https`) One of [ `http` | `https` ]. The only valid value for the TLS Tester is `https`.

#### `browser`

(Optional) One of [ `chrome` | `firefox` ]. If not present the default will be:

* For client-side validation: What is specified in the CLI config file under `sut.browser`
* For server-side validation: What is specified in the _orchestrator_ config file under `sut.browser`

The `browser` config values are passed into the `BrowserApp` _Job_ schema. Only used for the `BrowserApp` _Job_ schema.

#### `loggedInIndicator`

(Optional) Sub-string to expect in a future response indicating that the _Emissary_ was successfully authenticated.
See the [Zaproxy documentation](https://www.zaproxy.org/docs/desktop/start/features/authentication/) for additional details.
Conflicts with `loggedOutIndicator`

#### `loggedOutIndicator`

(Optional) Sub-string to expect as part of a future response indicating that the _Emissary_ is not currently authenticated.
See the [Zaproxy documentation](https://www.zaproxy.org/docs/desktop/start/features/authentication/) for additional details.
Conflicts with `loggedInIndicator`

### `relationships`

A [relationships object] containing a `data` ([resource linkage]) array of [resource identifier object]s. In the case of _PurpleTeam_, these refer to one of [ `tlsScanner` | `appScanner` | `serverScanner` ({{< hl >}}coming soon{{< /hl >}})] [resource object]s.

#### `data`

An array of [resource identifier object]s containing the following properties that identify the _Test Session_ [resource object]s to be included in the _Test Run_:

{{< highlight js >}}
{
  "type": ["tlsScanner"|"appScanner"],
  "id": String
}
{{< /highlight >}}

* `type` - One of [ `tlsScanner` | `appScanner` | `serverScanner` ({{< hl >}}coming soon{{< /hl >}})]
* `id` - The id of the `type` _Test Session_ [resource object] being included

</br>

---

## `included`

An array of [resource object]s.

Such as _Test Sessions_ (`tlsScanner`, `appScanner`, `serverScanner` ({{< hl >}}coming soon{{< /hl >}})) and `route`. 

### `tlsScanner`

The only valid number of `tlsScanner` [resource object]s is one.

This is handled in the _orchestrator's_ _Tester_ models. A meaningful error will be generated and returned to the CLI if this number is not correct. Part of the _orchestrator's_ initialisation phase of each _Tester_ is to validate that the number of their _Test Session_ [resource object]s fits within the allowed range. If they don't then none of the _Testers_ will be started.

#### `type`

The only valid value is `tlsScanner` which defines the [resource object].

#### `id`

The only valid value is `NA`.

#### `attributes`

An [attributes object] containing the following:

`tlsScannerSeverity`

(Optional) One of [ `LOW` | `MEDIUM` | `HIGH` | `CRITICAL` ].

If the TLS _Tester_ finds a severity equal to or higher than the `tlsScannerSeverity` value, then it will be logged to reports: CSV and JSON only, as well as to the Graphical CLI bug counter. `WARN` is another level which translates to a client-side scanning error or problem. `WARN` and `DEBUG` records will always be seen in all reports if they occur.

`INFO` and `OK` records are only logged to CSV and JSON reports if the `tlsScannerSeverity` property doesn't exist. They are however logged to LOG and HTML reports regardless.

`OK`, `INFO`, `WARN` and `DEBUG` records are not counted as defects.

All TLS _Tester_ actions get logged to LOG and HTML reports, as well as to the CLI output. The CLI log for the TLS _Tester_ is the same as the TLS _Tester_ report but with some extra details about whether or not the _Test Run_ was a pass or a fail.
If it was a fail the CLI log for the TLS _Tester_ will provide details around how many vulnerabilities exceeded the _Build User_ defined alert threshold.

`alertThreshold`

(Optional) If present it's value must be between 0 and 9999. This property is useful for expressing the number of defects you expect to be found, any defects over and above this number will fail the _Test Run_.
If the `alertThreshold` exists in the _Job_ file, it allows the _Build User_ to ignore alerts for the sake of a _Test Run_ passing.

### `appScanner`

The only valid number of `appScanner` [resource object]s is from 1 to 12 inclusive.

This is handled in the _orchestrator's_ _Tester_ models. A meaningful error will be generated and returned to the CLI if this number is not correct. Part of the _orchestrator's_ initialisation phase of each _Tester_ is to validate that the number of their _Test Session_ [resource object]s fits within the allowed range. If they don't then none of the _Testers_ will be started.

#### `type`

The only valid value is `appScanner` which defines the [resource object].

#### `id`

A `String` that conforms to the relevant regular expression in the _Job_ schema.

#### `attributes`

An [attributes object] containing the following:

`sitesTreePopulationStrategy`

(Optional, Default: `WebDriverStandard`) One of [ `WebDriverStandard` ]. The specific strategy used defines how the _Emissary_ sites tree is populated

`spiderStrategy`

(Optional, Default: `Standard`) One of [ `Standard` ]. The specific strategy used defines how the _Emissary_ spider scans the _SUT_

`scannersStrategy`

(Optional, Default: `BrowserAppStandard`) One of [ `BrowserAppStandard` ]. The specific strategy used defines how the _Emissary_ scanners are configured

`scanningStrategy`

(Optional, Default: `BrowserAppStandard`) One of [ `BrowserAppStandard` ]. The specific strategy used defines how the _Emissary_ carries out it's scanning. Using the default strategy for the `BrowserApp` schema both the _Emissary_'s spider and active scanner will scan all routes in a given _Test Session_ if there are any. If there are non then the base URL (`sutIp`) will be scanned recursively. This allows the _Build User_ to get either broad scans or narrow focussed scans. If you need a [different behaviour](../#strategy-properties) discuss with us what you need

`postScanningStrategy`

(Optional, Default: `BrowserAppStandard`) One of [ `BrowserAppStandard` ]. The specific strategy used defines how the _Emissary_ carries out it's immediate post scanning activities

`reportingStrategy`

(Optional, Default: `Standard`) One of [ `Standard` ]. The specific strategy used here defines how the _Emissary_ carries out it's post scanning reporting activities

`reports`

(Optional) object containing the single property `templateThemes` array. If not present all report types will be generated. See [templateThemes](#templatethemes) below for more details.

{{% callout note %}}
Just be aware that the AWS API Gateway only allows responses of 10MB maximum, if the outcomes archive is larger than that, then you will receive a generic error: `Error occurred while downloading the outcomes file, error was: purpleteam Cloud API responded with "orchestrator is down"` due to a `500` error from API Gateway. If this happens, reduce the set of reports you specify.
{{% /callout %}}

`username`

A `String` used to authenticate to your _SUT_ and/or for various other calls to the _Emissary_ API. Must conform to the relevant regular expression in the _Job_ schema.

`password`

(Optional) `String` used to authenticate to your _SUT_.

`aScannerAttackStrength`

(Optional, Default: `HIGH`) The default is specified in the App _Tester_ config file. One of [ `LOW` | `MEDIUM` | `HIGH` | `INSANE` ].

`aScannerAlertThreshold`

(Optional, Default: `LOW`) The default is specified in the App _Tester_ config file. One of [ `LOW` | `MEDIUM` | `HIGH` ]

`alertThreshold`

(Optional, Default: `0`) If present it's value must be between 0 and 9999. This property is useful for expressing the number of defects you expect to be found, any defects over and above this number will fail the _Test Run_.
If the `alertThreshold` exists in the _Job_ file, it allows the _Build User_ to ignore alerts for the sake of a _Test Run_ passing.

`excludedRoutes`

(Optional) Array of regular expression strings containing route sub-strings to be added to the _Emissary_'s "Exclude from Context" list.
These can be quite useful if you have decided to create an `appScanner` _Test Session_ with no associated `route` [resource object]s which means the App _Emissary_ will be scanning from `sutIp` recursively including all resources under it.

`updateAlertsConfidence` {{< hl >}}[coming soon](https://github.com/purpleteam-labs/purpleteam/issues/100){{< /hl >}}

(Optional) Array containing objects of [ `confidenceId` & `filter` ] properties. See [updateAlertsConfidence](#updatealertsconfidence) below for more details.

#### `relationships`

A [relationships object] containing a `data` ([resource linkage]) array of [resource identifier object]s. In the case of _PurpleTeam_, these refer to `route` [resource object]s.

`data`

An array of [resource identifier object]s containing the following properties:

{{< highlight js >}}
{
  "type": "route",  
  "id": String
}
{{< /highlight >}}

* `type` - Containing the value "route"
* `id` - The id of a route [resource object] to include in this `appScanner` _Test Session_ [resource object]

### `route`

None to many `route` [resource object]s can be present.

#### `type`

The only valid value is `route` which defines the [resource object].

#### `id`

A `String` that conforms to the relevant regular expression in the _Job_ schema.

#### `attributes`

An [attributes object] containing the following properties:

{{< highlight js >}}
  "attackFields": [
    {"name": String, "value": [String|Boolean|Number], "visible": Boolean}
  ],
  "method": ["GET"|"PUT"|"POST"],
  "submit": String
{{< /highlight >}}

* `attackField`s - Are optional objects with fields: `name`, `value`, (Optional) `visible`
* `method` - One of [ `GET` | `PUT` | `POST` ]
* `submit` - A `String` that is used to find the element on the page to submit. The `String` is used to search for the first occurrence of a DOM `id`, class name, or `name` that matches the given `String`

### `serverScanner`

{{< hl >}}comming soon{{< /hl >}}

</br>

---

## Sub Resources

The following resources are referenced from the above _Job_ schema.

### `templateThemes`

An array containing one or more of the following report meta data objects you expect to be generated by the _Emissary_.
This allows the _Build User_ to specify which report types they desire:

{{< highlight js >}}
{
  "name": String
}
{{< /highlight >}}

* `name` - One of [ `traditionalHtml` | `traditionalHtmlPlusLight` | `traditionalHtmlPlusDark` | `traditionalJson` | `traditionalMd` | `traditionalXml` | `riskConfidenceHtmlDark` | `modernMarketing` | `highLevelReport` ]

### `updateAlertsConfidence`

{{< hl >}}[coming soon](https://github.com/purpleteam-labs/purpleteam/issues/100){{< /hl >}}

An array containing none to many objects with the following properties. This allows the _Build User_ to define none to many sets of a filter to apply a confidence level to. A given alert raised by the _Emissary_ must have all of the properties matching a given filter in order for the specified confidence to be applied:

{{< highlight js >}}
"confidenceId": integer,
"filter": {
  "name": String,
  "cweid": String, 
  "wascid": String,
  "confidence": String,
  "risk": String,
  "description": String,
  "url": String
}
{{< /highlight >}}

* `confidenceId` - One of [ `0` | `1` | `2` | `3` | `4` ]. `0`: False Positive, `1`: Low, `2`: Medium, `3`: High, `4`: _Build User_ Confirmed
* `filter` - An object containing none to all of the following properties. None means there is no filter so act on all alerts:
  * `name` - (Optional) The name of the alert to filter on
  * `cweid` - (Optional) The cweid of the alert to filter on
  * `wascid` - (Optional) The wascid of the alert to filter on
  * `confidence` - (Optional) The existing confidence of the alert to filter on
  * `risk` - (Optional) The current risk of the alert to filter on
  * `description` - (Optional) The description of the alert to filter on
  * `url` - (Optional) The url of the alert to filter on






[resource object]: https://jsonapi.org/format/#document-resource-objects
[attributes object]: https://jsonapi.org/format/#document-resource-object-attributes
[relationships object]: https://jsonapi.org/format/#document-resource-object-relationships
[resource linkage]: https://jsonapi.org/format/#document-resource-object-linkage
[resource identifier object]: https://jsonapi.org/format/#document-resource-identifier-objects

