---
# Title, summary, and page position.
linktitle: Job File
summary: Details on the Job File types and their schemas.
weight: 90
#icon: building
#icon_pack: far

# Page metadata.
title: Job File
date: "2021-11-27T00:00:00Z"
type: book  # Do not modify.
---

You can think of the _Job_ file as the executable specificatiion defining what you want PurpleTeam to test and in some cases how.
The PurpleTeam CLI validates, filters and when necessary sanitises your _Job_ file before sending to the back-end (specifically the _orchestrator_), where it is again passed through the same validation, filtering and sanitisation process.
There is yet another schema for each _Tester_ that validates, filters and sanitises the parts of the _Job_ file that each specific _Tester_ works with.

There are two _Job_ file `type`s (`BrowserApp` and `Api`) with accompanying schemas. The _Job_ file `type` is specified within the top level `data` object:

{{< highlight js >}}
{
  "data": {
    "type": ["BrowserApp"|"Api"],
    ...
{{< /highlight >}}

* `type` - One of [ `BrowserApp` | `Api` ]

<br>

---

{{<list_children>}}

---

<br>

As you work through creating your first _Job_ file, remember to keep it as simple as possible to start with. Then build on it as you understand more.

* Examples can be found at the [CLI Github](https://github.com/purpleteam-labs/purpleteam/tree/main/testResources/jobs). Don't be afraid to ask another community member on one or both of the [PurpleTeam Slacks](/community) for examples
* The _Job_ file schemas conform to [jsonapi](https://jsonapi.org/)
* All properties are considered mandatory unless specified as optional
* Bear in mind that there are optional properties that are required by certain strategies. If you are unsure check the [strategy](https://github.com/purpleteam-labs/purpleteam-app-scanner/tree/main/src/sUtAndEmissaryStrategies) you intend on using as to whether or not it consumes the property in question. If you are still unsure ask on one of the PurpleTeam Slack channels
* For properties marked as Optional that have a default value: If the _Build User_ omits them, a validation message will be printed to the CUI (not in CLI logs) informing the _Build User_ of which optional property values were added
* The best way to test that your _Job_ file is valid is by running the CLI's `testplan` command as this command doesn't start a _Test Run_ and in-fact does very little, it will only return the _Testers_ test plans if your _Job_ file is valid. You can even run this command without the _PurpleTeam_ back-end running, if you run the CLI's `testplan` command and your _Job_ file is valid, you will see a blank screen followed by a message telling you that the _orchestrator_ is down, if your _Job_ file is not yet valid, you will receive error messages informing you of which parts of your _Job_ file are invalid.

## Strategy Properties

There are a number of properties in the _Job_ schema suffixed with `Strategy`. These values provide the _Build User_ an opportunity to define the App _Tester Emissary_'s behaviour. The code for these strategies is located in the app-scanner [repository](https://github.com/purpleteam-labs/purpleteam-app-scanner/tree/main/src/sUtAndEmissaryStrategies).
They are ordered in general sequence of execution as determined by the relevant (BrowserApp | Api) Cucumber [step definitions](https://github.com/purpleteam-labs/purpleteam-app-scanner/tree/main/src/steps).
If you find you need something different, discus what you need with us and/or submit a Github Pull Request.

## Regular Expressions

For _Job_ file property values that allow regular expressions, if you have access to the App _Tester Emissary_ (Zaproxy) running on your desktop as a GUI application you could use it's [regular expression tester](https://www.zaproxy.org/docs/desktop/addons/regular-expression-tester/) to verify that your regular expressions will work as expected.


