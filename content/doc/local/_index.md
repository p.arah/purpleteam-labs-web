---
# Title, summary, and page position.
linktitle: Local
summary: PurpleTeam local (OWASP) environment.
weight: 50
icon: building
icon_pack: far

# Page metadata.
title: Local
date: "2021-05-08T00:00:00Z"
type: book  # Do not modify.
---

<br>Everything in this sub-section applies to running the _PurpleTeam_ services in the `local` environment (on your desktop or server that you have access to). (AKA **OWASP _PurpleTeam_**)<br><br>

If running in the `cloud` environment most of this is unnecessary. You will still need your system under test (_SUT_) to be running and in a clean state before starting tests.

{{<list_children>}}
