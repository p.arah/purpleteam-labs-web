---
title: 'Set-up'
linktitle: └─ Set-up
summary: Components you will need to set-up if you want to run the entire PurpleTeam solution locally.
type: book
date: "2021-05-08T00:00:00+01:00"
toc: true
draft: false

## Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 50
---

The following are the components you will need to set-up if you want to run the entire _PurpleTeam_ solution `local`ly.

If you can't be bothered with all this work, then use the PurpleTeam-Labs `cloud` environment. Head to the [quick-start page](../../quick-start).

If you are using the `local` environment, run through all of the set-up steps in this file and the linked resources. All git repositories that you clone or fork locally should sit within the same directory level. Once you have completed the `local` set-up, continue to the [workflow](../workflow) page to get all of the _PurpleTeam_ components running `local`ly.

The path of least resistance is to set everything up on a GNU/Linux distribution. Windows tends to yield additional obstacles.

## PurpleTeam `local` Architecture

The following diagram shows how the _PurpleTeam_ components communicate. In the `local` environment it's your responsibility to follow all the directions to enable all components to interact correctly:

<!-- https://gohugo.io/content-management/shortcodes/#figure -->
{{< figure src="doc/purpleteam_local_2021-08-min.png" alt="local architecture" title="Local Architecture (click to enlarge)" >}}

At this stage we're locked to docker-compose versions before the rewrite of v2. This is mostly because we currently depend on docker-compose-ui which only supports docker-compose versions prior to version 2.

## Docker Network

Before most of the supporting commands can be run, such as running docker-compose-ui (which hosts the stage two containers), and sam-cli (which hosts the purpleteam-lambda functions `local`ly), the [`compose_pt-net`](https://github.com/purpleteam-labs/purpleteam-orchestrator/blob/main/compose/orchestrator-testers-compose.yml#L4) Docker network needs to be created.
You can create this bridge network manually. Alternatively once you have both purpleteam-orchestrator and purpleteam-app-scanner repository cloned and configured as per the details under [Orchestrator](#orchestrator) and [Application Scanner](#application-scanner), running  the following two commands from the purpleteam-orchestrator root directory will create the network:

```
npm run dc-build
```
```
npm run dc-up
```

Providing you have configured the _orchestrator_ and Application Scanner:

`dc-build` will build the stage one Docker services (images).  
`dc-up` will create the required Docker network and containers, it will then bring all containers up that are listed (stage one) in the [orchestrator-testers-compose.yml](https://github.com/purpleteam-labs/purpleteam-orchestrator/blob/main/compose/orchestrator-testers-compose.yml) file. The actual process of running the components is under the [Workflow](/doc/local/workflow/) page.

## Your System Under Test (_SUT_)

Obviously you are going to need a web application that you want to put under test. A couple of options for you to start to experiment with if you don't yet have a _SUT_ in a Docker container ready to be tested are:

1. If you want to target a local _SUT_, it needs to be running in a Docker container. Clone a copy of [NodeGoat](https://github.com/OWASP/NodeGoat) and make the following changes:  
   * Add the following docker-compose.override.yml file to the root directory  
{{< highlight yml >}}
version: "3.7"

networks:
  compose_pt-net:
    external: true

services:
  web:
    networks:
      compose_pt-net:
    depends_on:
      - mongo
    container_name: pt-sut-cont
    environment:
      - NODE_ENV=production
  mongo:
    networks:
      compose_pt-net:
{{< /highlight >}}
        This option means that NodeGoat will be running in the `pt-net` network created by the orchestrator's docker-compose
   * Change the passwords in the artifacts/db-reset.js file.  
   
   The PurpleTeam-labs core team usually just store these files in a sibling directory to the NodeGoat repository.
   
   When you are ready to bring NodeGoat up, just run the following command from the NodeGoat root directory:  
     ```shell
     docker-compose up
     ```
2. An alternative is to spin up [purpleteam-iac-sut](https://github.com/purpleteam-labs/purpleteam-iac-sut). PurpleTeam-Labs use this for both `local` and `cloud` testing

## Lambda functions

Details on installing and configuring the aws cli and aws-sam-cli in order to be able to host the purpleteam-lambda functions `local`ly can be found [here](https://github.com/purpleteam-labs/purpleteam-lambda)

## Stage Two containers

Details on configuring and debugging (gaining insights into what is happening inside the containers) if needed can be found [here](https://github.com/purpleteam-labs/purpleteam-s2-containers)

## Orchestrator

To run the stage one containers (_orchestrator_, _Testers_ and Redis) we use npm scripts from the purpleteam-orchestrator project to run the [docker-compose files](https://github.com/purpleteam-labs/purpleteam-orchestrator/tree/main/compose).
The main docker-compose file is [orchestrator-testers-compose.yml](https://github.com/purpleteam-labs/purpleteam-orchestrator/blob/main/compose/orchestrator-testers-compose.yml).

### Environment Variables

The orchestrator-testers-compose.yml file has several bind mounts. The mounts expect the `HOST_DIR` and `HOST_DIR_APP_SCANNER` environment variables to exist and be set to host directories of your choosing.
You will need source directories set-up and their respective directory paths assigned as values to the `HOST_DIR` and `HOST_DIR_APP_SCANNER` environment variables.

The directory that the `HOST_DIR` refers to is mounted by the _orchestrator_ and all _Testers_.
This directory gets written to by the _Testers_ and _orchestrator_ and read from by the _orchestrator_.
This directory should have `0o770` permissions.

The directory that the `HOST_DIR_APP_SCANNER` refers to is mounted by both App _Tester_ and it's _Emissary_.
The App _Tester_ writes files here that it's _Emissary_ consumes, the App _Emissary_ writes reports here that the App _Tester_ consumes.
This directory should have `0o770` permissions.

We have created a .env.example file in the _orchestrator_ compose/ directory. Rename this to .env and set any values within appropriately.

### Set-up for Running _Emissaries_

#### Firewall Rules

If you use a firewall, you may have to make sure that the _PurpleTeam_ components can communicate with each other.

1. [_Testers_](../../definitions/) need to communicate with the locally running Lambda service in order to start and stop [stage two containers](https://github.com/purpleteam-labs/purpleteam-s2-containers).  

  Communications (TCP) will need to flow from the [app-scanner](https://github.com/purpleteam-labs/purpleteam-app-scanner) container ([`pt-app-scanner-cont`](https://github.com/purpleteam-labs/purpleteam-orchestrator/blob/main/compose/orchestrator-testers-compose.yml#L40)) bound to the [`pt-net`](https://github.com/purpleteam-labs/purpleteam-orchestrator/blob/main/compose/orchestrator-testers-compose.yml#L4) (or listed as `compose_pt-net` with `docker network ls`) Docker network IP address of [`172.25.0.120`](https://github.com/purpleteam-labs/purpleteam-orchestrator/blob/main/compose/orchestrator-testers-compose.yml#L22) - to the IP address and port that local Lambda is listening on (`172.25.0.1:3001`) which can be seen in the `sam local start-lambda` commands (as seen in the [local-workflow documentation](../../local/workflow/)) used to host the lambda functions

#### Host IP Forwarding

Make sure host [IP forwarding](https://www.dedoimedo.com/computers/docker-networking.html#mozTocId387645) is [turned on](https://linuxconfig.org/how-to-turn-on-off-ip-forwarding-in-linux).

<br>

Details on installing the _orchestrator_ dependencies and configuring can be found  [here](https://github.com/purpleteam-labs/purpleteam-orchestrator).

## Testers

Currently _PurpleTeam_ has the app-scanner and the tls-scanner implemented. The server-scanner is stubbed out and in the [Product Backlog](https://github.com/purpleteam-labs/purpleteam/projects/2) waiting to be implemented. Details on progress can be found [here](https://github.com/purpleteam-labs/purpleteam/issues/61) (for server-scanner).

### Application Scanner

Details on installing the app-scanner dependencies and configuring can be found [here](https://github.com/purpleteam-labs/purpleteam-app-scanner)

### TLS Scanner

Details on installing the tls-scanner dependencies and configuring can be found  [here](https://github.com/purpleteam-labs/purpleteam-tls-scanner)

### Server Scanner

Not yet implemented.

Details on installing the server-scanner dependencies and configuring once implemented will be found [here](https://github.com/purpleteam-labs/purpleteam-server-scanner)

## PurpleTeam (CLI)

Details on installing the _PurpleTeam_ CLI (_purpleteam_), configuring and running can be found [here](https://github.com/purpleteam-labs/purpleteam)

<br>

Once completed the `local` set-up, head to the [workflow](../../local/workflow) page to get all of the _PurpleTeam_ components running `local`ly.
