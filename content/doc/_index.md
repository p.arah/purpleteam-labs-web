---
title: Documentation

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
#linktitle: Course

# Page summary for search engines.
summary: PurpleTeam documentation

# Date page published
date: 2021-05-08

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 1

type: book  # Do not modify.
toc: false
---

Welcome to the documentation for PurpleTeam!

{{<list_children>}}
