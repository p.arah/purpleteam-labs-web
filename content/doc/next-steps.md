---
title: Next Steps
linktitle: Next Steps
summary: Once you are comfortable with the basics, have worked through the Quick Start, have your first Job file configured, completed your first manual Test Run, and maybe started to add the PurpleTeam CLI to your build pipeline(s), continue with the Next Steps.
type: book
date: "2021-12-12T00:00:00+01:00"
toc: true
draft: false

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 30
---

The _Testers_ other than the App _Tester_ don't require any advanced configuration.

## Some Background

The App _Tester_ (project name [app-scanner](https://github.com/purpleteam-labs/purpleteam-app-scanner)) is responsible for driving all interactions with it's _Emissary_ Zaproxy. These interactions reside within a sequence of abstract strategies.
Each strategy needs a concrete strategy, the specific concrete strategies you decide to use are specified in your _Job_ file.

The sequence that the strategies execute in is ultimately defined by the cucumber feature file (the test plan). This is exactly what you see when you execute the CLI `testplan` command once you have your _Job_ file configured correctly. The `BrowserApp` (for example) feature file is located [here](https://github.com/purpleteam-labs/purpleteam-app-scanner/blob/main/src/features/BrowserApp/app_scan.feature).

The feature file is implemented with the [app_scan_steps.js](https://github.com/purpleteam-labs/purpleteam-app-scanner/blob/main/src/steps/BrowserApp/app_scan_steps.js). which as you can see lines up with the sequence of strategies below.

The strategies are defined in the [`sUtAndEmissaryStrategies`](https://github.com/purpleteam-labs/purpleteam-app-scanner/tree/main/src/sUtAndEmissaryStrategies) directory. They are the same for `BrowserApp` and `Api` type _Job_ files, and look like the following:

| |
|----------------------------------------|
| 1_sitesTreeSutAuthenticationPopulation |
| 2_sitesTreePopulation |
| 3_emissaryAuthentication |
| 4_spider |
| 5_scanners |
| 6_scanning |
| 7_postScanning |
| 8_reporting |

You will notice in the CLI App _Tester_ terminal when a _Job_ is running that it mentions the particular method of which strategy is currently executing. For example these are the relevant log entries of an arbitrary `BrowserApp` _Job_ when in progress, which also show the specific concrete strategy for each abstract strategy you have defined in your _Job_ file:

{{< highlight text >}}

Tester initialised.
...
The authenticate() method of the SitesTreeSutAuthenticationPopulation strategy "Link" has been invoked.
...
The populate() method of the SitesTreePopulation strategy "WebDriverStandard" has been invoked.
...
The configure() method of the EmissaryAuthentication strategy "ScriptLink" has been invoked.
...
The scan() method of the Spider strategy "Standard" has been invoked.
...
The configure() method of the Scanners strategy "BrowserAppStandard" has been invoked.
...
The scan() method of the Scanning strategy "BrowserAppStandard" has been invoked.
...
The process() method of the PostScanning strategy "BrowserAppStandard" has been invoked.
...
The createReports() method of the Reporting strategy "Standard" has been invoked.
...
{{< /highlight >}}


We recommend that the _PurpleTeam_ documentation pertaining to the App _Tester_ be read in conjunction with the Zaproxy documentation.

Let's have a look at the "_SUT_ and _Emissary_ Strategies". Our intention is that the code is understandable enough to work out what's going on in each strategy. If you can't work something out, please ask us and we'll try and make the area of code in question easier to understand.

Some concrete strategies can be used for both `BrowserApp` and `Api` , some are specific to either `BrowserApp` or `Api`.

## 1. `SitesTreeSutAuthenticationPopulation`

This strategy is responsible for populating the Zap sites tree with the URLs that Zap will need to know about in order to authenticate to your SUT.

## 2. `SitesTreePopulation`

This strategy is responsible for populating the Zap sites tree with the URLs that Zap will need to know about in order to perform scanning.

## 3. `EmissaryAuthentication`

Authentication is often the most complicated area of application scanning, that's why we try and work around it if at all possibly in order to test the core functionality of your application and/or API.

We support various types of authentication and are adding new and modified strategies as we go. Usually the easiest way to set-up your authentication is to download Zap and have it running on your desktop. Walk through the sequence of strategies manually performing the interactions (mostly calls to Zap API) that you see in code... with instead Zap on the desktop, ultimately this is what PurpleTeam does anyway. This will allow you to interact with Zap and be able to view the Zap responses, logs, errors, etc. If you do this you will start to get a good idea of what PurpleTeam is doing during a _Test Run_.

The wrapper library of Zap API calls can be found [here](https://github.com/purpleteam-labs/purpleteam-app-scanner/blob/main/src/emissaries/zAp.js). These show the actual path to the Zap API being invoked. If you are running Zap on the desktop you can browse to all of the API endpoints.

It's usually easiest to start with an existing strategy and instead manually drive Zap with your mouse or perform the same actions using Zap's comprehensive API. Doing the later will ensure that when you codify the Zap API requests they will work as you experienced with Zap on the desktop. By executing API calls via your browser, curl, wget, etc, you can witness the changes in the Zap desktop GUI.

When you need to load or create a new javascript to plug into Zap, be sure to checkout the Zap [community scripts](https://github.com/zaproxy/community-scripts) and the [zap-in-ten](https://www.zaproxy.org/zap-in-ten/) videos.

The most useful Zaproxy documentation resources we've used are listed in the strategy source code files themselves.

## 4. `Spider`

Currently the `Spider` strategy is empty, the Spidering is now performed in the `Scanning` strategy.

## 5. `Scanners`

This strategy enables/configures the scanners.

## 6. `Scanning`

This strategy is responsible for the scanning of your application or API.

## 7. `PostScanning`

This strategy is responsible for any post scanning activities such as notifications or anything else that needs to be done.

## 8. `Reporting`

This strategy is responsible for all the work around deleting reports from previous _Test Sessions_, generating, formatting and moving reports and their supporting artefacts to a location accessible by the _orchestrator_ so that they can be archived along with all _Tester_ [result and report files](/doc/log-and-outcomes-files/#outcomes-files) and sent to the location defined in the CLI configuration.

