---
# An instance of the Featurette widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: featurette

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Interactive or Headless
subtitle: <br>You decide whether to run the CLI in interactive mode or headless.<br><br>

# Showcase personal skills or business features.
# - Add/remove as many `feature` blocks below as you like.
# - For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
- description: Provides real-time feedback on what all of the _Testers_ are doing and finding. At the end of the _Test Run_ the _Outcomes_ archive is sent to a directory of your choosing.
  icon: terminal
  icon_pack: fas
  name: Interactive
- description: Allows the CLI process to be embedded in your build process or any process for that matter. At the end of the _Test Run_ the _Outcomes_ archive is sent to a directory of your choosing.
  icon: server
  icon_pack: fas
  name: Headless


# Uncomment to use emoji icons.
#- icon = ":smile:"
#  icon_pack = "emoji"
#  name = "Emojiness"
#  description = "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
#- icon = "your-custom-icon-name"
#  icon_pack = "custom"
#  name = "Surfing"
#  description = "90%"

advanced:
  # Custom CSS.
  css_style: ""

  # CSS class.
  css_class: "pt-featurette-col-66"
---
