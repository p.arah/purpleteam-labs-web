+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 70  # Order that this section will appear.

title = "Getting Started"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-product-additional-resources"
+++

<!-- 8. 2nd CTA -->
<!-- 9. Urgency -->

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body pt-main">
        &nbsp;
        <div class="row">
          <div class="col-md-12 col-lg-6">
            <div class="card">
              <div class="card-body">
                <h3 class="card-title text-center">
                  Test Your Application
                </h3>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-book-reader" aria-hidden="true"></i>&nbsp;</span><span>Review pricing page</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-book-reader" aria-hidden="true"></i>&nbsp;</span><span>Review documentation</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-map-signs" aria-hidden="true"></i>&nbsp;</span><span>Decide on <code>cloud</code> or <code>local</code> environment</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-cloud" aria-hidden="true" style="color: #9B6BCC"></i>&nbsp;</span><span>If <code>cloud</code>, contact us for onboarding details</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-server" aria-hidden="true"></i>&nbsp;</span><span>If <code>local</code>, head to the documentation</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-terminal" aria-hidden="true"></i>&nbsp;</span><span><a href="https://github.com/purpleteam-labs/purpleteam#contents" target="_blank">Configure</a> the purpleteam CLI</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-vial" aria-hidden="true"></i>&nbsp;</span><span><a href="https://github.com/purpleteam-labs/purpleteam#run" target="_blank">Start testing</a> your application</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-lg-6">
            <div class="card">
              <div class="card-body">
                <h3 class="card-title text-center">
                  Test OWASP NodeGoat
                </h3>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-truck" aria-hidden="true"></i>&nbsp;</span><span>Deploy <a href="https://github.com/OWASP/NodeGoat" target="_blank">NodeGoat</a> as <a href="/doc/definitions/" target="_blank">SUT</a> using <a href="https://github.com/purpleteam-labs/purpleteam-iac-sut" target="_blank">iac-sut</a></span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-book-reader" aria-hidden="true"></i>&nbsp;</span><span>Review pricing page</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-book-reader" aria-hidden="true"></i>&nbsp;</span><span>Review documentation</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-map-signs" aria-hidden="true"></i>&nbsp;</span><span>Decide on <code>cloud</code> or <code>local</code> environment</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-cloud" aria-hidden="true" style="color: #9B6BCC"></i>&nbsp;</span><span>If <code>cloud</code>, contact us for onboarding details</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-server" aria-hidden="true"></i>&nbsp;</span><span>If <code>local</code>, head to the documentation</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-terminal" aria-hidden="true"></i>&nbsp;</span><span><a href="https://github.com/purpleteam-labs/purpleteam#contents" target="_blank">Configure</a> the purpleteam CLI</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">
                    <span><i class="fas fa-vial" aria-hidden="true"></i>&nbsp;</span><span><a href="https://github.com/purpleteam-labs/purpleteam#run" target="_blank">Start testing</a> NodeGoat with PurpleTeam</span>
                  </li>
                  <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <a href="/doc/quick-start" target="_blank" class="btn btn-danger"><i class="fas fa-running" aria-hidden="true"></i>&nbsp;Quick Start</a>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <a href="/contact/" target="_blank" class="btn btn-dark"><i class="fas fa-phone" aria-hidden="true" style="color: white;"></i>&nbsp;Contact Us</a>
        </div>
      </div>
    </div>
  </div>
</div>
