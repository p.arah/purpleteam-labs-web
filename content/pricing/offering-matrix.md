+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "Choose the plan that’s right for you"
subtitle = "<br>Offering Matrix"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-pricing-offering-matrix"
+++

<!-- Example of tooltips are in publications. Each publication has author_notes, these are displayed as a tooltip with info icon. The actual implementation is in partial page_metadata-authors.html -->

<div class="container text-center">
  <div class="row">
    <div class="col">
      <table class="table table-sm shadow">
        <thead class="">
          <tr class="pt-standard-row">
            <th scope="col"></th>
            <th scope="col" style="padding: 0.6rem 1rem 0.2rem 1rem">
              <b>Community</b>
              <br>
              $0/month
              <a href="/doc/local/" class="btn btn-block btn-sm btn-danger" style="padding: 4px 0 3px 0; margin: 10px auto 10px;">Get&nbsp;Started</a>
            </th>
            <th scope="col" style="padding: 0.6rem 1rem 0.2rem 1rem">
              <b>Standard</b>
              <br>
              $85/month
              <a href="/contact/" class="btn btn-block btn-sm pt-cta-btn-standard" style="padding: 4px 0 3px 0; margin: 10px auto 10px;">Get&nbsp;Started</a>
            </th>
            <th scope="col" style="padding: 0.6rem 1rem 0.2rem 1rem">
              <b>Pro</b>
              <br>
              $185/month
              <a href="/contact/" class="btn btn-block btn-sm btn-primary" style="padding: 4px 0 3px 0; margin: 10px auto 10px;">Get&nbsp;Started</a>
            </th>
            <th scope="col" style="padding: 0.6rem 1rem 0.2rem 1rem">
              <b>Enterprise</b>
              <br>
              Custom
              <a href="/contact/" class="btn btn-block btn-sm btn-dark pt-cta-btn-enterprise" style="padding: 4px 0 3px 0; margin: 10px auto 10px;">Get&nbsp;Started</a>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr class="pt-heading-row">
            <th scope="row" class="pt-left-column">Outcomes</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="The PurpleTeam CLI provides feedback on concurrent Test Sessions in real-time.">Real-time <em>Tester</em> feedback</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Sent from the PurpleTeam API within an Outcomes archive.">Test Results persisted locally</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Sent from the PurpleTeam API within an Outcomes archive.">Detailed Test Reports (multiple formats)</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="The Application Scanner provides a report with directions of how to reproduce each test failure.">Reproduction directions</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-heading-row">
            <th scope="row" class="pt-left-column">Source Code</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Full source code available at https://github.com/purpleteam-labs">PurpleTeam CLI (open source)</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Full source code available at https://github.com/purpleteam-labs">PurpleTeam API (open source)</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="OWASP PurpleTeam">OWASP Project&nbsp;&nbsp;&nbsp;<a href="https://owasp.org/www-project-purpleteam/" target="_blank"><i class="fas fa-link" aria-hidden="true"></i></a></th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-heading-row">
            <th scope="row" class="pt-left-column">Location</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">PurpleTeam API all set-up</th>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">locally hosted</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">PurpleTeam <code>cloud</code></th>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="We supply you with a static IP address that the PurpleTeam back-end is attached to. Great for enterprise environments that require vendor IP allow listing.">Static IP</th>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-heading-row">
            <th scope="row" class="pt-left-column">Debugging</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Full details on how to debug each component.">All components&nbsp;&nbsp;&nbsp;<a href="/doc/local/workflow/#debugging" target="_blank"><i class="fas fa-link" aria-hidden="true"></i></a></th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-heading-row">
            <th scope="row" class="pt-left-column">Smart Orchestration</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Test Sessions are parallelized.">Parallelization</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Every customers isolated environment is load balanced.">Load Balancing</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" title="">Full Tenant Isolation</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" title="">Full <em>Test Session</em> isolation</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">CI/CD automation</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Handy for brown fields projects that have known existing security defects that you do not want to count towards new bugs.">Set your own defect thresholds</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Set your own Attack Strength</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Set your own Alert Thresholds</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Useful for when you are scanning broadly and want to exclude certain routes of your web application or API.">Set your own route exclusions</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="We provide a collection of filters you can apply to your Job file to hide or change the confidence level of any given alert.">False positive alert filtering&nbsp;&nbsp;&nbsp;<a href="/doc/jobfile/browser-app/#updatealertsconfidence" target="_blank"><i class="fas fa-link" aria-hidden="true"></i></a></th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-heading-row">
            <th scope="row" class="pt-left-column">Testers</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Application</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">SSL/TLS</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Server <a href="https://github.com/purpleteam-labs/purpleteam/issues/61" target="_blank">coming soon</a></th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="PurpleTeam has been architected to easily add new Testers.">Add your own</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-heading-row">
            <th scope="row" class="pt-left-column">Usage</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Unlimited scan minutes per day</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">360 scan minutes per week day</th>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">480 scan minutes per every day</th>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Customer must prove target domain and/or application ownership.">Unlimited scan targets</th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Up to 4 concurrent <em>Test Sessions</em></th>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Up to 8 concurrent <em>Test Sessions</em></th>
            <td></td>            
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Up to 12 concurrent <em>Test Sessions</em></th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>          
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Allows your scan minutes to be split into multiple scheduled blocks.">Split schedules</th>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>          
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Limited to two routes for two hours per day.">Free 2 week Trial</th>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-heading-row">
            <th scope="row" class="pt-left-column">Support</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Github Discussions">Github Discussions&nbsp;&nbsp;&nbsp;<a href="https://github.com/purpleteam-labs/purpleteam/discussions" target="_blank"><i class="fas fa-link" aria-hidden="true"></i></a></th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Github Issues">Github Issues&nbsp;&nbsp;&nbsp;<a href="https://github.com/purpleteam-labs/purpleteam/issues" target="_blank"><i class="fas fa-link" aria-hidden="true"></i></a></th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="Open Web Application Security Project - Slack channel for purpleteam">Community (OWASP) Slack&nbsp;&nbsp;&nbsp;<a href="https://app.slack.com/client/T04T40NHX/C01LARX6WP8" target="_blank"><i class="fas fa-link" aria-hidden="true"></i></a></th>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Private (BinaryMist) Slack</th>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="All of the BinaryMist publications become available to you, including paid for books, podcasts, conference talks, blog posts, etc.">BinaryMist Publications&nbsp;&nbsp;&nbsp;<a href="https://binarymist.io/publication/kims-selected-publications/" target="_blank" ><i class="fas fa-link" aria-hidden="true"></i></a></th>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="During business hours as detailed on the contact page.">Email (48 hour response)</th>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="During business hours as detailed on the contact page.">Email (24 hour response)</th>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column">Premium Phone Support</th>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
          <tr class="pt-standard-row">
            <th scope="row" class="pt-left-column" data-toggle="tooltip" style="cursor: help;" title="The security expertise of the PurpleTeam-Labs core team is available to you for one-on-one collaboration.">Collaboration on test findings</th>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-check" aria-hidden="true"></i></td>
          </tr>
        </tbody>
      </table>
      <br>
      See <a href="/doc/definitions" target="_blank">definitions</a> for further explanation of common PurpleTeam terms.&nbsp;&nbsp;&nbsp;All prices in USD.
    </div>
  </div>
</div>

