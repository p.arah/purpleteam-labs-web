+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "Application and Infrastructure Security Within Your Dev Team"
subtitle = "<br>Free 2 week trial."

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-pricing-tiers"
+++

<!-- For css use: https://getbootstrap.com/docs/3.4/css/ -->
<!-- We're using bootstrap v4.6.0 -->

<!-- In order to debug custom scripts, you need to put them in the /static/js/ directory and reference them as per the below line -->
<!-- <script language="javascript" type="text/javascript"  src="/js/pt-custom.js"></script> -->
<!-- Also mentioned in assets/js/READ.txt -->

<!-- This is also copied and adapted for the home page -->

<div class="container text-center">
  <div class="row">
    <div class="col">
      <p class="text-center anual-indicator">Showing prices for anual billing</p>
      <p class="text-center monthly-indicator" hidden>Showing prices for monthly billing</p>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="custom-control custom-switch custom-switch-lg">
        <input type="checkbox" class="custom-control-input" id="pricing-monthly-vs-anual" checked data-size="lg">
        <label class="custom-control-label" for="pricing-monthly-vs-anual"></label>
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-xs-1 col-md-6 col-lg-6 col-xl-3">
      <div class="card">
        <!--<img src="owasp.png" class="card-img-top" alt="free forever">-->
        {{< figure src="pricing/PT_plan_owasp.svg" theme="light" alt="free forever" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">Community</h3>
        </div>
        <ul class="list-group list-group-flush pt-price">
          <li class="list-group-item card-title">
            <h1 class="card-title">Free</h1>
            <small>&nbsp;<br>hosted locally</small>
          </li>
        </ul>
        <div class="card-body">
          <a href="/doc/local/" class="btn btn-danger btn-block">Get Started</a>
          <ul class="list-group list-group-flush">
            <li class="list-group-item text-left pt-what-you-get" style="margin-top: 1.2rem;">
              <span class="font-weight-bold">Usage</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span><span>Unlimited scans</span>
            </li>            
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Unlimited targets</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>12 concurrent <em>Test Sessions</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get">
              <span class="font-weight-bold">Features</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>OWASP Top 10 +</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Isolated <em>Testers</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Application <em>Tester</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>SSL/TLS <em>Tester</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Server <em>Tester</em> <a href="https://github.com/purpleteam-labs/purpleteam/issues/61" target="_blank">soon</a></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Add your own <em>Tester</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>CI/CD automation</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Scan <em>Outcomes</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Repro directions</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Defect Thresholds</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get">
              <span class="font-weight-bold">Support</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Community Slack</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Github Issues</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Github Discussions</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Full documentation</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Full source code</div>
            </li>
          </ul>
          <br>
          <a href="/doc/local/" class="btn btn-danger btn-block">Get Started</a>
        </div>
      </div>
    </div>
    <div class="col-xs-1 col-md-6 col-lg-6 col-xl-3">
      <div class="card">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Standard Pricing">-->
        {{< figure src="pricing/PT_plan_standard.svg" theme="light" alt="Standard Pricing" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">Standard</h3>
        </div>
        <ul class="list-group list-group-flush pt-price pt-standard">
          <li class="list-group-item card-title">
            <h1 class="card-title pt-anual">$85</h1>
            <h1 class="card-title pt-monthly" hidden>$110</h1>
            <small>USD per month<br>hosted in cloud</small>
          </li>
        </ul>
        <div class="card-body">
          <a href="/contact/" class="btn btn-block pt-cta-btn-standard">Get Started</a>
          <ul class="list-group list-group-flush">
            <li class="list-group-item text-left pt-what-you-get" style="margin-top: 1.2rem;">
              <span class="font-weight-bold">Usage</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span><span>360 scan mins per day</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span><span>Unlimited targets</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>4 concurrent <em>Test Sessions</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get">
              <span class="font-weight-bold">Features</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Community Features</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div> PurpleTeam <code>cloud</code></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get">
              <span class="font-weight-bold">Support</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Community Support</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Private Slack</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>BM Publications</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
          </ul>
          <br>
          <a href="/contact/" class="btn btn-block pt-cta-btn-standard">Start Trial</a>
        </div>
      </div>
    </div>
    <div class="col-xs-1 col-md-6 col-lg-6 col-xl-3">
      <div class="card">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Pro Pricing">-->
        {{< figure src="pricing/PT_plan_pro.svg" theme="light" alt="Pro Pricing" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">Pro</h3>
        </div>
        <ul class="list-group list-group-flush pt-price pt-pro">
          <li class="list-group-item card-title">
            <h1 class="card-title pt-anual">$185</h1>
            <h1 class="card-title pt-monthly" hidden>$240</h1>
            <small>USD per month<br>hosted in cloud</small>
          </li>
        </ul>
        <div class="card-body">
          <a href="/contact/" class="btn btn-primary btn-block">Get Started</a>
          <ul class="list-group list-group-flush">
            <li class="list-group-item text-left pt-what-you-get" style="margin-top: 1.2rem;">
              <span class="font-weight-bold">Usage</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span><span>480 scan mins per day</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span><span>Unlimited targets</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>8 concurrent <em>Test Sessions</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get">
              <span class="font-weight-bold">Features</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Standard Features</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div> PurpleTeam <code>cloud</code></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get">
              <span class="font-weight-bold">Support</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Standard Support</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>48 hr response Email</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
          </ul>
          <br>
          <a href="/contact/" class="btn btn-primary btn-block">Start Trial</a>
        </div>
      </div>
    </div>
    <div class="col-xs-1 col-md-6 col-lg-6 col-xl-3">
      <div class="card">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Custom Pricing">-->
        {{< figure src="pricing/PT_plan_enterprise.svg" theme="light" alt="Custom Pricing" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">Enterprise</h3>
        </div>
        <ul class="list-group list-group-flush pt-price">
          <li class="list-group-item card-title">
            <h2 class="card-title">Custom Pricing</h2>
            <div style="height: 2.16rem;"></div>
            <small>hosted in cloud</small>
          </li>
        </ul>
        <div class="card-body">
          <a href="/contact/" class="btn btn-block btn-dark pt-cta-btn-enterprise">Let's Talk</a>
          <ul class="list-group list-group-flush">
            <li class="list-group-item text-left pt-what-you-get" style="margin-top: 1.2rem;">
              <span class="font-weight-bold">Usage</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span><span>Unlimited scan mins</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span><span>Unlimited targets</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>12 concurrent <em>Test Sessions</em></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get">
              <span class="font-weight-bold">Features</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Pro Features</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div> PurpleTeam <code>cloud</code></div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
            <li class="list-group-item text-left pt-what-you-get">
              <span class="font-weight-bold">Support</span>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Pro Support</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Findings collaboration</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>Phone Support</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <i class="fa fa-check" aria-hidden="true"></i><div>24 hr response Email</div>
            </li>
            <li class="list-group-item text-left pt-what-you-get pt-line-item">&nbsp;</li>
          </ul>
          <br>
          <a href="/contact/" class="btn btn-block btn-dark pt-cta-btn-enterprise">Contact Us</a>
        </div>
      </div>
    </div>
  </div>
</div>

