---
# An instance of the Blank widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: blank

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 10

# Section title
title: Community

# Section subtitle
subtitle:

# Section design
design:
  # Use a 1-column layout
  columns: "1"
  #background:
  #  gradient_start: '#dc3545'
  #  gradient_end: '#007bff'
---

# Get In Touch

* [Github Discussions](https://github.com/purpleteam-labs/purpleteam/discussions) - A good option to get a discussion happening
* [OWASP PurpleTeam Slack channel](https://app.slack.com/client/T04T40NHX/C01LARX6WP8) (#project-purpleteam) - If you need an invite to the OWASP Slack, ask for one [here](https://owasp.org/slack/invite)
* purpleteam-labs Slack - for those on paid plans
* [Contact form and/or Email](https://purpleteam-labs.com/contact/)
* [DM on Twitter](https://twitter.com/purpleteamlabs/) - @purpleteamlabs

# Report Problems

* [Github Issues](https://github.com/purpleteam-labs/purpleteam/issues) and the [Product Backlog](https://github.com/purpleteam-labs/purpleteam/projects/2) we work from
* [Security Bugs](/security-policy/) - Details on how to report security issues

# Get Involved

* [Contributing Guide](https://github.com/purpleteam-labs/purpleteam/blob/main/CONTRIBUTING.md) - Getting started, what we expect from you and what you can expect from us in return
* [Source Code](https://github.com/purpleteam-labs) - All of the public PurpleTeam repositories

