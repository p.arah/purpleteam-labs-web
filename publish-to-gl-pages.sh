#!/bin/bash
# Run from public

# In order to set-up the orphan gl-pages branch:
# Create a new orphan branch: git checkout --orphan gl-pages
# Clean all (untracked) files: git reset --hard or git rm -rf .
# Create first commit: git commit --allow-empty -m "Initializing gl-pages branch"
# Push the new branch on your repo: git push origin gl-pages
# Get back to your main branch: git checkout main
# Create the worktree in a public folder (public/ needs to not exist before running this): git worktree add -B gl-pages public origin/gl-pages

if [[ $(git status -s) ]]
then
    echo "The working directory is dirty. Please commit any pending changes."
    exit 1;
fi

cd ..

echo "Deleting old publication"
rm -rf public
mkdir public
git worktree prune
rm -rf .git/worktrees/public/

echo "Checking out gl-pages branch into public"
git worktree add -B gl-pages public origin/gl-pages

echo "Removing existing files"
rm -rf public/*

echo "Generating site"
hugo

echo "Updating gl-pages branch"
echo "cd'ing into public"
cd public

echo "Adding back the gitlab build file after deletion."
cp ../.gitlab-ci.yml .
#echo "Adding back the google search site verification after deletion."
#echo google-site-verification: google7fae6e706755d64a.html > google7fae6e706755d64a.html

echo "git add'ing all to staging"
git add --all
echo "git commit'ing"
git commit -m "Publishing to gl-pages (publish-to-gl-pages.sh)"



echo "Do you wish to push this commit?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) git push origin gl-pages; break;;
        No ) exit;;
    esac
done


