# Changelog

## v4.0.0-alpha.3

All notable changes to this project will be documented here.  
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).  
Releases follow [Semantic Versioning](https://semver.org/).

### [Unreleased]

https://gitlab.com/purpleteam-labs/purpleteam-labs-web/-/compare/v4.0.0-alpha.3...main?from_project_id=9346212

### Added

* Add changelog e979439feb06d3249ccae0dbe2b077539131b255
* Add note about docker-compose version b5cdb04cf7e487cc97baacc63f1046a4f9a7b32e
* Add strategy details to next steps page 4636b84545a50401e56c6780cfce11986d1fcfac
* Add keep outcomes.zip under 10MB warning to Job doc 81b556d6cbd2613e13cd35fdb3d0252c7b78d32c
* Add website to changelog a5797fe8f9e6d43114967d59e3347ee811fc431b

### Fixed

* Fix diff 36c7bb627b30f4b3c2ffbcaa6aa1e152007917dc

### Changed

* Reorder doc pages 094dd17cba3a2c5d36f71a97f51ba98eb751d861
* Update release strategies 8a30e9ec720a543786c083ebc3e5952990803b53
* Update log-and-outcomes doc with new report info bf62974dacdc140fc3ad603b701dea65e56f029f
* Update env vars doc for permissions for mounted dirs bc2e1ff8079d2ddb950c6f9e20131cb558ae10b4
* Update job schema with new reports property ea53505774548c2776627b6c580ceb50eb1bdbf1

https://gitlab.com/purpleteam-labs/purpleteam-labs-web/-/compare/v3.1.0-alpha.3...v4.0.0-alpha.3?from_project_id=9346212

---

## v3.1.0-alpha.3



### Added

* 

### Changed

* 

### Depricated

* 

### Removed

* 

### Fixed

* 

### Security

* 

https://gitlab.com/purpleteam-labs/purpleteam-labs-web/-/compare/v3.0.0-alpha.3...v3.1.0-alpha.3?from_project_id=9346212
